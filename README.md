# tail_framework

This project provides an easily accessible interface for adding or reading the clouds resources + keys

## Installation

This application is managed by the project lerring-framework and installed by setup. 

Use [docker-compose](https://docs.docker.com/compose/) to run the application manually.

```bash
$ docker-compose up --build -d 
```

## Usage
Swagger shall be included at a later date
