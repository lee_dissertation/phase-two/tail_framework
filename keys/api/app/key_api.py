import mysql.connector
import json
import configparser
import time

from lib.conf_reader import ConfReader
from lib.api.db_conn import MySQLPool

class KeyApi():

	def __init__(self):
		config=ConfReader('Db.properties','Db')
		dbconfig = {
			"host":config.getProperty('db.host'),
			"port":config.getProperty('db.port'),
			"user":config.getProperty('db.user'),
			"password":config.getProperty('db.password'),
			"database":config.getProperty('db.database')
		}
		self.pool = MySQLPool(**dbconfig,pool_size=30)

	def getKey(self,macAddr=None):
		"""
		Gets keys from the database
		Param:
			mac address to query by
		Return:
			result from select statement parsed into json
		"""
		sql="SELECT S.`MAC_ADDR` AS `KEY_MAC_ADDR`, S.`PEM`AS 'KEY_PEM' FROM keys.SIGNATURE AS S"
		if macAddr:
			sql+=" WHERE MAC_ADDR = %s"
			results=self.pool.execute(sql,(macAddr,))
		else:
			results=self.pool.execute(sql)
		keys=[]
		for result in results:
			key={}
			key['mac']=result['KEY_MAC_ADDR']
			key['pem']=result['KEY_PEM']
			keys.append(key)
		return keys

	def addKey(self,key):
		"""
		Inserts or replaces a signature key
		Param:
			key containing mac address and pem
		Returns:
			inserted key
		"""
		if 'pem' not in key or 'mac' not in key:
			return {'error':'malformed json in request'}

		sql="REPLACE INTO keys.SIGNATURE(MAC_ADDR, PEM) VALUES(%s,%s)"
		val=(key['mac'],key['pem'])
		self.pool.addTransaction(sql,val)
		self.pool.commitTransactions()
		return key
