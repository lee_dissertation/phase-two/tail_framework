import falcon
from key_api import KeyApi
from lib.api.server import EndPoint, Server
import json

class Keys(EndPoint):

	def on_get(self, req, resp):
		"""
		Gets the keys
		Param:
			optional mac address
		Returns:
			json response from api
		"""
		resp.status = falcon.HTTP_200
		macAddr=req.get_param('mac')
		resp.body = EndPoint.jsonResponse(self.api.getKey(macAddr))

	def on_put(self,req,resp):
		"""
		Puts keys
		Param:
			json featuring a pem and mac field
		Returns:
			key which has been added
		"""
		try:
			posted_data = json.loads(req.stream.read())
			resp.status = falcon.HTTP_200
			resp.body = EndPoint.jsonResponse(self.api.addKey(posted_data))
		except json.decoder.JSONDecodeError as e:
			resp.status = falcon.HTTP_400
			resp.body = EndPoint.jsonResponse({ 'error' : 'expects json in request body' })

class KeyServer(Server):

	def __init__(self):
		super().__init__([])
		api=KeyApi()
		self.app.add_route(r'/', Keys(api))

keyServer=KeyServer()
app=keyServer.getApp()

