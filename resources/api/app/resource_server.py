import falcon
import json

from resource_api import ResourceApi
from lib.api.server import EndPoint, Server

"""
Resources is a single endpoint which defines both get and put requests to access the resource database.
"""
class Resources(EndPoint):
	def on_get(self, req, resp):
        """
        GET with optional parameter of 'node' to specify a list of nodes requested
        """
		resp.status = falcon.HTTP_200
		node = req.get_param_as_list('node')
		resp.body = self.api.getHosts(node)
	def on_put(self,req,resp):
        """
        PUT for the Reporter to send data to the server as a JSON structure
        """
		resp.status = falcon.HTTP_200
		posted_data = json.loads(req.stream.read())
		resp.body = self.api.addHost(posted_data)

"""
ResourceServer represents the API as a runnable instance
"""
class ResourceServer(Server):

	def __init__(self):
		super().__init__([])
		api=ResourceApi()
		self.app.add_route(r'/',Resources(api))
#create app instance for gunicorn
resourceServer=ResourceServer()
app=resourceServer.getApp()
