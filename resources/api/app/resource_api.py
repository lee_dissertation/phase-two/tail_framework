import json

from lib.conf_reader import ConfReader
from lib.api.db_conn import MySQLPool

class ResourceApi():

	def __init__(self):
		config=ConfReader('Db.properties','Db')
		dbconfig = {
			"host":config.getProperty('db.host'),
			"port":config.getProperty('db.port'),
			"user":config.getProperty('db.user'),
			"password":config.getProperty('db.password'),
			"database":config.getProperty('db.database')
		}
		self.pool = MySQLPool(**dbconfig)

	def getHosts(self,host=None):
        """
        Performs a select statement on the database with optional parameters
        """
		sql="SELECT DISTINCT H.`MAC_ADDR` AS `HOST_MAC_ADDR`,H.`HOSTNAME`AS 'HOST_HOSTNAME', H.`STAMP` AS 'HOST_STAMP', H.`STATUS` AS 'HOST_STATUS', C.`CORES` AS 'CPU_CORES', CO.`NUM` AS 'CORE_NUM', CO.`TEMP` AS 'CORE_TEMP', CO.`USAGE` AS 'CORE_USAGE', CO.`FREQ_CURR` AS 'FREQ_CURR', CO.`FREQ_MAX` AS 'FREQ_MAX', CO.`FREQ_MIN` AS 'FREQ_MIN' , M.`TOTAL` AS 'MEM_TOTAL'  , M.`AVAILABLE` AS 'MEM_AVAILABLE'  , M.`USED` AS 'MEM_USED'  , M.`PERCENT` AS 'MEM_PERCENT'  , S.`FREE` AS 'SWP_AVAILABLE'  , S.`USED` AS 'SWP_USED'  , S.`TOTAL` AS 'SWP_TOTAL'  , S.`PERCENT` AS 'SWP_PERCENT' FROM resources.HOST AS H INNER JOIN resources.CPU AS C ON C.MAC_ADDR = H.MAC_ADDR AND C.STAMP = H.STAMP INNER JOIN resources.CORE AS CO ON CO.MAC_ADDR = H.MAC_ADDR AND CO.STAMP = H.STAMP LEFT JOIN resources.MEMORY AS M	   ON M.MAC_ADDR = H.MAC_ADDR  AND M.STAMP = H.STAMP LEFT JOIN resources.SWP AS S  ON S.MAC_ADDR = H.MAC_ADDR  AND S.STAMP = H.STAMP RIGHT JOIN( SELECT MAC_ADDR, MAX(MX.`STAMP`) AS STAMP FROM resources.HOST AS MX GROUP BY MX.MAC_ADDR ) AS HS ON H.`MAC_ADDR` = HS.`MAC_ADDR` AND H.`STAMP` = HS.`STAMP`"
		if host:
			sql+=" WHERE HOSTNAME IN (%s)"
			results=self.pool.execute(sql,tuple(host))
		else:
			results=self.pool.execute(sql)
		nodes={}
		for result in results:
			mac=result['HOST_MAC_ADDR']
			hostname=result['HOST_HOSTNAME']
			#create node if not existing in response
			if hostname not in nodes:
				newNode={}
				newNode['hostname']=hostname
				nodes[hostname]=newNode
			#get corresponding node 
			node=nodes[hostname]
			node['mac']=mac
			node['hostname']=hostname
			node['last_updated']=result['HOST_STAMP']
			node['status']=result['HOST_STATUS']
			#initialise objects
			if 'cpu' not in node:
				node['cpu']=self.getCpu(result)
			core_num = result['CORE_NUM']
			if not any(core.get('num',None)==core_num for core in node['cpu']['cores']):
				node['cpu']['cores'].append(self.getCore(result))
			if 'memory' not in node:
				node['memory']=self.getMem(result)
			if 'swp' not in node:
				node['swp']=self.getSwp(result)
		return json.dumps(nodes)

	def getCpu(self,result):
		"""
		Reads the result set and parses into JSON
		"""
		cpu={}
		cpu['count'] = result['CPU_CORES']
		cpu['cores']=[]
		return cpu

	def getCore(self,result):
		"""
		Reads the result set and parses it into JSON
		"""
		core={}
		core['num']=result['CORE_NUM']
		core['temp']=result['CORE_TEMP']
		core['usage']=result['CORE_USAGE']
		freq={}
		freq['curr']=result['FREQ_CURR']
		freq['max']=result['FREQ_MAX']
		freq['min']=result['FREQ_MIN']
		core['freq'] = freq
		return core

	def getMem(self,result):
		"""
		Reads the result set and parses it into JSON
		"""
		memory = {}
		memory['available'] = result['MEM_AVAILABLE']
		memory['used'] = result['MEM_USED']
		memory['total'] = result['MEM_TOTAL']
		memory['percent'] = result['MEM_PERCENT']
		return memory

	def getSwp(self,result):
		"""
		Reads the result set and parses it into JSON
		"""
		swp = {}
		swp['available'] = result['SWP_AVAILABLE']
		swp['used'] = result['SWP_USED']
		swp['total'] = result['SWP_TOTAL']
		swp['percent'] = result['SWP_PERCENT']
		return swp

	def addHost(self,node):
		"""
		Reads the JSON and parses into a query
		"""
		sql="INSERT INTO resources.HOST(MAC_ADDR,HOSTNAME,STAMP,STATUS) VALUES(%s,%s,%s,%s)"
		val=(node['mac'],node['hostname'], node['stamp'],'active')
		self.pool.addTransaction(sql,val)
		self.addCpu(node['mac'],node['stamp'],node['cpu'])
		self.addMemory(node['mac'],node['stamp'],node['memory'])
		self.addSwp(node['mac'],node['stamp'],node['swp'])
		self.pool.commitTransactions()

	def addCpu(self,mac, stamp, cpu):
		"""
		Reads the JSON and parses into a query
		"""
		sql="INSERT INTO resources.CPU(MAC_ADDR,STAMP,CORES) VALUES (%s,%s,%s)"
		val=(mac,stamp,cpu['count'])
		self.pool.addTransaction(sql,val)
		for core in cpu['cores']:
			self.addCore(mac,stamp,core)

	def addCore(self,mac,stamp,core):
		"""
		Reads the JSON and parses into a query
		"""
		sql="INSERT INTO resources.CORE(MAC_ADDR,STAMP,NUM,TEMP,`USAGE`,FREQ_CURR,FREQ_MAX,FREQ_MIN) VALUES(%s,%s,%s,%s,%s,%s,%s,%s)"
		val=(mac,stamp,core['num'],core['temp'],core['usage'],core['freq']['current'],core['freq']['max'],core['freq']['min'])
		self.pool.addTransaction(sql,val)

	def addMemory(self,mac, stamp, mem):
		"""
		Reads the JSON and parses into a query
		"""
		sql="INSERT INTO resources.MEMORY(MAC_ADDR,STAMP,AVAILABLE,USED,TOTAL,PERCENT) VALUES (%s,%s,%s,%s,%s,%s)"
		val=(mac,stamp,mem['available'],mem['used'],mem['total'],mem['percent'])
		self.pool.addTransaction(sql,val)

	def addSwp(self,mac, stamp, swp):
		"""
		Reads the JSON and parses into a query
		"""
		sql="INSERT INTO resources.SWP(MAC_ADDR,STAMP,TOTAL,USED,FREE,PERCENT) VALUES (%s,%s,%s,%s,%s,%s)"
		val=(mac,stamp,swp['total'],swp['used'],swp['free'],swp['percent'])
		self.pool.addTransaction(sql,val)
